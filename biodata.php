<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>latihan 1</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="berita.html">Berita</a>
            </li>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="galeri.html">Galeri</a>
        </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="kontak.html">Kontak</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-4 p-3">
            <div class="card">
                <div class="card-body">
                    <img src="idris.jpg" class="img-fluid" alt="Idris" srcset="">

                </div>
            </div>  
        </div>
        <div class="col-md-8 p-3">
            <div class="card">
                <div class="card-body bg-warning">
                    <table class="table table-hover table-warning"> 
                        <thead>
                            <h1 class="text-center  bg-warning"><i>Biodata Diri</i></h1>
                        </thead>
                        <tbody class="rtl">
                </div>
                <div class="card-body bg-warning">
                    <table class="table table-hover table-warning"> 
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>Idris Efendi</td>
                            </tr>
                            <tr>
                                <td>NPM</td>
                                <td>:</td>
                                <td>011220011</td>
                            </tr>
                            <tr>
                                <td>Prodi</td>
                                <td>:</td>
                                <td>Informatika</td>    
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td>Kota Tanah</td>
                            </tr> 
                            <tr>
                                <td>Tempat Tanggal Lahir</td>
                                <td>:</td>
                                <td>Oku Timur,14 Mei 2004</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>Laki Laki</td>
                            </tr>  
                            <tr>
                                <td>Agama</td>
                                <td>:</td>
                                <td>Islam</td>
                            </tr> 
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td>Mahasiswa</td>
                            </tr> 
                            <tr>
                                <td>Kewarganegaraan</td>
                                <td>:</td>
                                <td>Indonesia</td>
                                <tr>
                                    <td>Hobi</td>
                                    <td>:</td>
                                    <td>
                                        <ul>
                                            <li>Casting</li>
                                            <li>Ngebolang</li>
                                            <li>Bulutangkis</li>
                                        </ul>
                                        <tr>
                                            <td>Riwayat Pendidikan</td>
                                            <td>:</td>
                                            <td>
                                        <ol>
                                            <li>SD Negeri 1 Semendwai Timur</li>
                                            <li>SMP Negeri 1 Semendawai Timur</li>
                                            <li>SMA Negeri 1 Semendawai Suku III</li>
                                        </ol>
                                    </td>
                                </tr> 
                            </tr> 
                        </tbody>
                    </table>  
                </div>
            </div>
        
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>